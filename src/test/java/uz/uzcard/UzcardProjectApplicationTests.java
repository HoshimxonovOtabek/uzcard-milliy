package uz.uzcard;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import uz.uzcard.entity.TransactionEntity;
import uz.uzcard.repository.TransactionRepository;
import uz.uzcard.util.CardNumberGenerator;
import uz.uzcard.service.DocumentService;


import java.util.ArrayList;
import java.util.Collection;

@SpringBootTest
class UzcardProjectApplicationTests {


	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private DocumentService documentService;


	@Autowired
	private CardNumberGenerator numberGenerator;

	@Test
	void contextLoads() {

		String generate = numberGenerator.generate("8600", 16);
		String s = numberGenerator.hideCardNumber(generate);
		System.out.println("s = " + s);

	}


	@Test
	void pdf() {
		Iterable<TransactionEntity> all = transactionRepository.findAll();
		ArrayList<TransactionEntity> list = new ArrayList<TransactionEntity>((Collection<TransactionEntity>) all);
		//writeDoc.printPdf(list.stream().toList());


	}

}
