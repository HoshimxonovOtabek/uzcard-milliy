package uz.uzcard.service;


import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Component;
import uz.uzcard.dto.FileDTO;
import uz.uzcard.entity.TransactionEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class DocumentService {
    private static final String PATH = "src/main/resources/";
    public static final String DEST ="Transactions list-"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-dd-MMMM|HH:mm:ss"))+".pdf";

    public FileDTO transactionListToPDF(List<TransactionEntity> list) {
        try {
            File file = new File(PATH + DEST);
            file.getParentFile().mkdirs();
            Document document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(PATH + DEST));
            document.open();
            float[] columnWidths = {5, 2, 5, 5, 3, 5, 2};
            PdfPTable table = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setUseAscender(true);
            Font f = new Font(FontFamily.HELVETICA, 25, Font.BOLD, GrayColor.GRAYWHITE);
            PdfPCell cell = new PdfPCell(new Phrase(" TRANSACTION LIST ", f));
            cell.setBorderColor(BaseColor.WHITE);
            cell.setBackgroundColor(GrayColor.BLACK);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(7);
            table.addCell(cell);
            table.getDefaultCell().setBackgroundColor(new GrayColor(0.75f));
            table.normalizeHeadersFooters();
            table.addCell("ID ");
            table.addCell("AMOUNT");
            table.addCell("CARD ID");
            table.addCell("CREATED DATE");
            table.addCell("STATUS ");
            table.addCell("TRANSFER ID");
            table.addCell("TYPE ");
            table.setHeaderRows(8);
            table.setFooterRows(2);
            table.getDefaultCell().setBackgroundColor(GrayColor.GRAYWHITE);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            for (TransactionEntity customer : list) {
                table.addCell(String.valueOf(customer.getId()));
                table.addCell(String.valueOf(customer.getAmount()));
                table.addCell(customer.getCardId());
                LocalDateTime createdDate = customer.getCreatedDate();
                table.addCell("" + createdDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd / HH:mm:ss")));
                table.addCell(String.valueOf(customer.getStatus()));
                table.addCell(String.valueOf(customer.getTransferId()));
                table.addCell(String.valueOf(customer.getType()));
            }
            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FileDTO(PATH,DEST);
    }
}