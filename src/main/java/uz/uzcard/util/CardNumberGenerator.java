package uz.uzcard.util;

import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CardNumberGenerator {

    private Random random = new Random(System.currentTimeMillis());

    public String generate(String bin, int length) {
        int randomNumberLength = length - (bin.length() + 1);

        StringBuilder builder = new StringBuilder(bin);
        for (int i = 0; i < randomNumberLength; i++) {
            int digit = this.random.nextInt(10);
            builder.append(digit);
        }
        int checkDigit = this.getCheckDigit(builder.toString());
        builder.append(checkDigit);

        return builder.toString();
    }


    private int getCheckDigit(String number) {

        int sum = 0;
        for (int i = 0; i < number.length(); i++) {
            int digit = Integer.parseInt(number.substring(i, (i + 1)));

            if ((i % 2) == 0) {
                digit = digit * 2;
                if (digit > 9) {
                    digit = (digit / 10) + (digit % 10);
                }
            }
            sum += digit;
        }
        int mod = sum % 10;
        return ((mod == 0) ? 0 : 10 - mod);
    }

    public static String hideCardNumber(String cardNumber) {

        String result = "";
        Pattern compile = Pattern.compile("[0-9]{16}");
        Matcher matcher=compile.matcher(cardNumber);
        while (matcher.find()){
            String group = matcher.group();
            result=result +group.substring(0,4)+group.substring(5,13).replaceAll("[0-9]","*")+group.substring(12,group.length());
            System.out.println(result);
        }

         return result ;

    }



}
