package uz.uzcard.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.uzcard.dto.TransactionFilterDTO;
import uz.uzcard.entity.ClientEntity;
import uz.uzcard.entity.TransactionEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class TransactionFilterRepository {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    public List<TransactionEntity> filter(TransactionFilterDTO filter) {

        StringBuilder builder = new StringBuilder();
        builder.append(" select t.* from transaction t  ");
        builder.append(" inner join card cd on cd.id=t.card_id ");
        builder.append(" where t.status ='SUCCESS' ");
        return getList(filter, builder);
    }


    private List<TransactionEntity> getList(TransactionFilterDTO dto, StringBuilder builder) {
        if (dto.getClientId() != null) {
            builder.append(" and cd.client_id = '" + dto.getClientId() + "' ");
        }
        if (dto.getCardNumber() != null) {
            builder.append(" and cd.number = '" + dto.getCardNumber() + "' ");
        }
        if (dto.getCardId() != null) {
            builder.append(" and cd.id = '" + dto.getCardId() + "' ");
        }
        if (dto.getFromAmount() != null) {
            builder.append(" and t.amount > '" + dto.getFromAmount() + "' ");
        }
        if (dto.getToAmount() != null) {
            builder.append(" and t.amount < '" + dto.getToAmount() + "' ");
        }
        if (dto.getFromDate() != null && dto.getToDate()!=null) {
            builder.append(" and t.created_date between '" + dto.getFromDate() + "' and '" + dto.getToDate() +"' ");
        }




        Query query = entityManager.createNativeQuery(builder.toString(), ClientEntity.class);


        return (List<TransactionEntity>) query.getResultList();
    }
}
