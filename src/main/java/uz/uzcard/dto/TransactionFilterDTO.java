package uz.uzcard.dto;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class TransactionFilterDTO {

    private String clientId;
    private String cardNumber;
    private String cardId;
    private Double fromAmount;
    private Double toAmount;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String profileName;
    private String status;


}
