package uz.uzcard.dto;

import lombok.Data;

@Data
public class FileDTO {

    private String path;
    private String fileName;

    public FileDTO() {
    }

    public FileDTO(String path, String fileName) {
        this.path = path;
        this.fileName = fileName;
    }
}
