FROM openjdk:18-jdk-slim-buster AS build
WORKDIR /opt/app
COPY target/uzcard-milliy-demo.jar app.jar
CMD ["java","-jar","app.jar"]
